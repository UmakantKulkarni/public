<img src="others/images/purdue-cs-logo.jpg" alt="drawing" width="450"/>

# CS 422: Computer Networks (Spring 2022)

[[_TOC_]]

## Logistics

- Instructor: [Muhammad Shahbaz](https://mshahbaz.gitlab.io/)
- Teaching assistants: 
  - [Danushka Menikkumbura](https://www.cs.purdue.edu/people/graduate-students/dmenikku.html)
  - [Rakin Haider](https://sites.google.com/site/rakinhaider/)
- Lecture time: **MW 6:30-7:45pm**
- Location: LWSN B155
- Credit Hours: 3.00
- Course discussion and announcements: [Campuswire](https://campuswire.com/p/G41B154B2) 
- Paper reviews: [Perusall](https://app.perusall.com/courses/spring-2022-cs-42200-le2-lec/_/dashboard)
- Development environment: [AWS Academy](https://awsacademy.instructure.com/courses/12909)
- Exam submission: [Gradescope](https://www.gradescope.com/courses/347756)
- Office hours
  - Monday 3:00-4:00pm, [Zoom](https://purdue-edu.zoom.us/j/92876453645?pwd=WW0reFhPdmhHMkRmTzR4c3lmay9Wdz09), Muhammad Shahbaz
  - Thursday 4:00-5:00pm, [WebEx](https://purdue-student.webex.com/meet/dmenikku), Danushka Menikkumbura
  - Wednesday 4:30-5:30pm, [Zoom](https://purdue-edu.zoom.us/j/99602095648?pwd=QUxqdXQ2SVE5bXNIK2pGNWpoYll5QT09), Rakin Haider
- Practice study observation (PSO), LWSN B158
  - Mondays 4:30pm-5:20pm | 5:30pm-6:20pm, Danushka Menikkumbura
  - Tuesdays 5:30pm-6:20PM | 6:30pm-7:20pm, Rakin Haider

> **Note:** Visit [Brightspace](https://purdue.brightspace.com/d2l/home/515741) for instructions on joining [Campuswire](https://campuswire.com/p/G41B154B2), [Gradescope](https://www.gradescope.com/courses/347756), [Perusall](https://app.perusall.com/courses/spring-2022-cs-42200-le2-lec/_/dashboard), and [AWS Academy](https://awsacademy.instructure.com/courses/12909).

#### Suggesting edits to the course page and more ...

We strongly welcome any changes, updates, or corrections to the course page or assignments or else that you may have. Please submit them using the [GitLab merge request workflow](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html).

## Course Description

CS 422 is an undergraduate-level course in Computer Networks at Purdue University. In this course, we will explore the underlying principles and design decisions that have enabled the Internet to (inter)connect billions of people and trillions of things on and off this planet---especially under the current circumstances marred by COVID-19. We will study the pros and cons of the current Internet design, ranging from classical problems (e.g., packet switching, routing, naming, transport, and congestion control) to emerging and future trends, like data centers, software-defined networking (SDN), programmable data planes, and network function virtualization (NFV) to name a few.

The goals for this course are:

- To become familiar with the classical and emerging problems in networking and their solutions.
- To learn what's the state-of-the-art in networking research: network architecture, protocols, and systems.
- To gain some practice in reading research papers and critically understanding others' research.
- To gain experience with network programming using industry-standard and state-of-the-art networking platforms.

# Course Syllabus and Schedule

> **Notes:** 
> - This syllabus and schedule is preliminary and subject to change.
> - Everything is due at 11:59 PM (Eastern) on the given day.
> - Abbreviations refer to the following:
>   - KR: Kurose/Ross (6th edition)
>   - PD: Peterson/Davie (online version)
>   - SDN: Peterson/Cascone/O’Connor/Vachuska/Davie (online version)
>   - 5G: Peterson/Sunay (online version)


| Date    | Topics  | Notes | Readings |
| :------ | :------ | :------  | :------ |
| **Week 1** | **Course Overview** | | |
| Mon <br> Jan 10 | Introduction ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/8645778/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4458815&srcou=515741)) | | &bull; PD: [1.1 - 1.2 (Applications, Requirements)](https://book.systemsapproach.org/foundation.html) |
| Wed <br> Jan 12 | A Brief History of the Internet ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/8690313/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4458816&srcou=515741)) | | &bull; [How to Read a Paper](https://gitlab.com/purdue-cs422/spring-2022/public/-/raw/main/readings/HowToRead2017.pdf) <br> &bull; [Internet History](https://www.internetsociety.org/internet/history-internet/brief-history-internet/) (Optional)|
| **Week 2** | **Network Building Blocks** | | |
| Mon <br> Jan 17 | *Martin Luther King Jr. Day*: No Class | | |
| Wed <br> Jan 19 | Layering and Protocols ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/8725343/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4494093&srcou=515741)) | | &bull; PD: [1.3 (Architecture)](https://book.systemsapproach.org/foundation/architecture.html) <br> &bull; [End-to-End Arguments](https://gitlab.com/purdue-cs422/spring-2022/public/-/raw/main/readings/e2eArgument84.pdf) (Optional) |
| **Week 3** | **The Network API** | | |
| Mon <br> Jan 24 | Sockets: The Network Interface ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/8743846/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4515993&srcou=515741), [demo](demos/Sockets)) | | &bull; PD: [1.4 (Software)](https://book.systemsapproach.org/foundation/software.html) <br> &bull; [Beej's Guide](http://beej.us/guide/bgnet/) (Optional) |
| Wed <br> Jan 26 | Assignments Walkthrough and Overview ([video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4528178&srcou=515741)) | &bull; [Assignment 0](assignments/assignment0) <br> &bull; [AWS Academy HowTo](https://gitlab.com/purdue-cs422/spring-2022/public/-/raw/main/assignments/cs422-awsacademy-hotwo.pdf) | |
| **Week 4** | **Local Area Networks I** | | |
| Mon <br> Jan 31 | Direct Links: The Wire Interface ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/8772140/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4545377&srcou=515741)) | | &bull; PD: [2.1 - 2.6 (Technology, Encoding, Framing, ...)](https://book.systemsapproach.org/direct.html) |
| Tue <br> Feb 01 | | &bull; [Quiz #1](https://www.gradescope.com/courses/347756/assignments/1822366/submissions) `due Feb 02` | |
| Wed <br> Feb 02 | Direct Links: The Wireless Interface | &bull; *No class due to Winter Storm Warning*!  | &bull; PD: [2.7 - 2.8 (Wireless and Access Networks)](https://book.systemsapproach.org/direct.html) |
| **Week 5** | **Local Area Networks II** | | |
| Mon <br> Feb 07 | Indirect Links: L2 Switching ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/8804990/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4581780&srcou=515741)) | &bull; [Assignment 1](assignments/assignment1) `due Feb 18` <br> &bull; [Paper Review 1](https://app.perusall.com/courses/spring-2022-cs-42200-le2-lec/_/dashboard/assignments/ZJb8gGamdqvfrWFaX) `due Mar 07` | &bull; PD: [3.1 - 3.2 (Switching, Ethernet)](https://book.systemsapproach.org/internetworking.html) |
| Wed <br> Feb 09 | Indirect Links: L3 Switching ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/8813514/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4592159&srcou=515741), [demo](demos/ARP)) | | &bull; PD: [3.3.1, 3.3.2, 3.3.6 (Inernetwork, Service Model, ARP)](https://book.systemsapproach.org/internetworking.html) |
| **Week 6** | **Network Addressing and Configuration** | | |
| Mon <br> Feb 14 | Flat/Classful Addressing: Ethernet, IP, and Subnets ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/8833065/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4612281&srcou=515741)) | | &bull; PD: [3.3.3 - 3.3.9 (Addressing, DHCP ...)](https://book.systemsapproach.org/internetworking.html) |
| Wed <br> Feb 16 | Classless Addressing: CIDR ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/8840175/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4619404&srcou=515741))  | | &bull; PD: [3.3.5 (Classless Addressing)](https://book.systemsapproach.org/internetworking.html) |
| Thu <br> Feb 17 | | &bull; [Quiz #2]() `due Feb 18` | |
| **Week 7** | **Process-to-Process Communication** | | |
| Mon <br> Feb 21 | Transport: Reliable Delivery ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/8996766/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4644742&srcou=515741)) | &bull; [Assignment 2](assignments/assignment2) `due Mar 08` | &bull; PD: [2.5 (Reliable Transmission)](https://book.systemsapproach.org/direct/reliable.html) <br> &bull; PD: [5.1 - 5.2 (UDP, TCP)](https://book.systemsapproach.org/e2e.html) |
| Wed <br> Feb 23 | Transport: Flow Control ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/9002501/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4652011&srcou=515741)) | &bull;  *Midterm Review* | &bull; PD: [5.2 (TCP)](https://book.systemsapproach.org/e2e.html) <br> &bull; PD: [5.3 - 5.4 (RPC, RTP)](https://book.systemsapproach.org/e2e.html) (Optional)  |
| **Week 8** | **Software-Defined Networks (and Network Control Planes)** | | |
| Mon <br> Feb 28 | *Midterm Exam* | | |
| Wed <br> Mar 02 | Control-Plane Abstractions and OpenFlow ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/9038034/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4687860&srcou=515741)) | | &bull; How SDN will Shape Networking ([video](https://www.youtube.com/watch?v=c9-K5O_qYgA)) <br> &bull; SDN: [3: Basic Architecture](https://sdn.systemsapproach.org/arch.html) |
| **Week 9** | **Wide Area Networks I** | | |
| Mon <br> Mar 07 | Direct Networks: Intradomain Routing I ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/9049162/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4700057&srcou=515741)) <br> *Midterm Exam Discussion* | | PD: [3.4.2 (Routing: Distance Vector)](https://book.systemsapproach.org/internetworking/routing.html#distance-vector-rip) |
| Wed <br> Mar 09 | Direct Networks: Intradomain Routing II ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/9052854/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4703891&srcou=515741)) | &bull; [Assignment 3](assignments/assignment3) `due Mar 29` <br> &bull; [Paper Review 2](https://app.perusall.com/courses/spring-2022-cs-42200-le2-lec/queue14-359644190?assignmentId=zy8qcqSYTXBmrjtPJ&part=1) `due Apr 25` | PD: [3.4.3 (Routing: Link State)](https://book.systemsapproach.org/internetworking/routing.html#link-state-ospf) |
| Thu <br> Mar 10 | | &bull; [Quiz #3](https://www.gradescope.com/courses/347756/assignments/1920763) `due Mar 11` | |
| **Week 10** | **Spring Break** | | |
| **Week 11** | **Resource Allocation I** | | |
| Mon <br> Mar 21 | Congestion Control and Queuing Disciplines ([ppt](https://purdue.brightspace.com/d2l/le/content/515741/viewContent/9087915/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=515741&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-4735189&srcou=515741)) | | PD: [6.1 - 6.2 (Issues, Queuing)](https://book.systemsapproach.org/congestion.html) |
| Wed <br> Mar 23 | Transport: Congestion Control (TCP) | | PD: [6.3 - 6.4 (TCP, ...)](https://book.systemsapproach.org/congestion.html) |
| Fri <br> Mar 25 | | &bull; [Quiz #4]() `due Mar 28` | |
| **Week 12** | **Programmable Networks (and Network Data Planes)** | | |
| Mon <br> Mar 28 | TBD | | |
| Wed <br> Mar 30 | TBD | &bull; [Assignment 4]() `due Apr 12` | |
| **Week 13** | **Wide Area Networks II** | | |
| Mon <br> Apr 04 | TBD | | |
| Wed <br> Apr 06 | TBD | | |
| Thu <br> Apr 07 | | &bull; [Quiz #5]() `due Apr 08` | |
| **Week 14** | **Resource Allocation II** | | |
| Mon <br> Apr 11 | TBD | | |
| Wed <br> Apr 13 | TBD | &bull; [Assignment 5]() `due Apr 26` | |
| **Week 15** | **Datacenter Networks and Cloud**  | | |
| Mon <br> Apr 18 | TBD | | |
| Wed <br> Apr 20 | TBD | | |
| Thu <br> Apr 21 | | &bull; [Quiz #6]() `due Apr 22` | |
| **Week 16** | **Cellular Networks and 5G** | | |
| Mon <br> Apr 25 | *Guest Lecture by [Larry Peterson](https://en.wikipedia.org/wiki/Larry_L._Peterson)!* :tada: | Change of location: **LWSN 1142** | |
| Wed <br> Apr 26 | TBD | | |
| **Week 17** | **Exam Week** | | |
| Wed <br> May 04 | *Final Exam* | | |

## Prerequisites

This course assumes that students have a basic understanding of data structures and algorithms and experience with programming languages like C/C++ and Python. Please see [CS 240](https://selfservice.mypurdue.purdue.edu/prod/bwckctlg.p_disp_course_detail?cat_term_in=202120&subj_code_in=CS&crse_numb_in=24000), [CS 380](https://selfservice.mypurdue.purdue.edu/prod/bwckctlg.p_disp_course_detail?cat_term_in=202120&subj_code_in=CS&crse_numb_in=38003), or similar courses at Purdue for reference.

## Recommended Textbooks
- Computer Networking: A Top-Down Approach by J. Kurose and K. Ross (6th Edition)
- Computer Networks: A Systems Approach by L. Peterson and B. Davie ([Online Version](https://book.systemsapproach.org/index.html))
- Software-Defined Networks: A Systems Approach by L. Peterson, C. Cascone, B. O’Connor, T. Vachuska, and Bruce Davie ([Online Version](https://sdn.systemsapproach.org/index.html))
- 5G Mobile Networks: A Systems Approach by L. Peterson and O. Sunay ([Online Version](https://5g.systemsapproach.org/index.html))

> Other optional but interesting resources: [Sytems Approach - Blog](https://www.systemsapproach.org/blog), [TCP Congestion Control: A Systems Approach](https://tcpcc.systemsapproach.org/index.html), and [Operating an Edge Cloud: A Systems Approach](https://ops.systemsapproach.org)

## Paper Reading and Discussion

During the course, we will be reading and discussing **two** research papers on topics ranging from network protocols, systems, and architectures. You should closely read each paper and add comments and questions along with a summary (5 lines or so) of the paper on [Perusall](https://app.perusall.com/courses/spring-2022-cs-42200-le2-lec/_/dashboard) by the due date below. Please plan to provide at least five comments or questions for each paper on Perusall and follow the comments from other students and the course staff. 

> **Note:** General tips on reading papers are [here](https://gitlab.com/purdue-cs422/spring-2022/public/-/raw/main/readings/HowToRead2017.pdf). 

Grades for your class participation and paper reviews will be determined based on attendance and, more importantly, contributions to paper discussions on Perusall.

<!-- > **Note:** What we expect you to know and prepare before each discussion is [here](reading-papers.md). -->

### Reading list
- [Paper 1: A Protocol for Packet Network Intercommunication](https://app.perusall.com/courses/spring-2022-cs-42200-le2-lec/_/dashboard/assignments/ZJb8gGamdqvfrWFaX) `due Mar 07`
- [Paper 2: The Road to SDN: An Intellectual History of Programmable Networks](https://app.perusall.com/courses/spring-2022-cs-42200-le2-lec/queue14-359644190?assignmentId=zy8qcqSYTXBmrjtPJ&part=1) `due Apr 25`


## Programming Assignments

- [Assignment 0: Virtual Networks using Mininet and ONOS](assignments/assignment0) `not graded`
- [Assignment 1: File and Message Transmission using Sockets](assignments/assignment1) `due Feb 18`
- [Assignment 2: From Bridging to Switching with VLANs](assignments/assignment2) `due Mar 08`
- [Assignment 3: DNS Reflection Attacks' Detection and Mitigation](assignments/assignment3) `due Mar 29`
- [Assignment 4]() `due Apr 12`
- [Assignment 5]() `due Apr 26`

## Quizzes

- [Quiz #1](https://www.gradescope.com/courses/347756/assignments/1822366/submissions): Topics from weeks 1-3 `due Feb 02`
- [Quiz #2](https://www.gradescope.com/courses/347756/assignments/1868925/submissions): Topics from weeks 4-6 `due Feb 18`
- [Quiz #3](https://www.gradescope.com/courses/347756/assignments/1920763/submissions): Topics from weeks 7-9 `due Mar 11`
- [Quiz #4](): `due Mar 28`
- [Quiz #5](): `due Apr 08`
- [Quiz #6](): `due Apr 22`

> Format: take home, open book

## Midterm and Final Exams
There will be one midterm and a final exam based on course content (lectures, assignments, and paper readings).

- Midterm Exam `on Feb 28` 
- Final Exam `on May 04` 

> Format: in class, open book

## Grading

- Class participation and discussions: 10%
- Programming assignments: 35%
- Quizzes: 20%
- Midterm exam: 15%
- Final exam: 20%

## Policies

### Late submission

- Grace period: 24 hours for the entire semester.
- After the grace period, 25% off for every 24 hours late, rounded up.

If you have extenuating circumstances that result in an assignment being late, please let us know about them as soon as possible.

### Academic integrity

We will default to Purdue's academic policies throughout this course unless stated otherwise. You are responsible for reading the pages linked below and will be held accountable for their contents.
- http://spaf.cerias.purdue.edu/integrity.html
- http://spaf.cerias.purdue.edu/cpolicy.html

### Honor code

By taking this course, you agree to take the [Purdue Honors Pledge](https://www.purdue.edu/odos/osrr/honor-pledge/about.html): "As a boilermaker pursuing academic excellence, I pledge to be honest and true in all that I do. Accountable together - we are Purdue."

### COVID-19 and quarantine + isolation!

Please visit [Protect Purdue Plan](https://protect.purdue.edu/plan/) or [Spring 2022 resources](https://www.purdue.edu/innovativelearning/teaching-remotely/resources.aspx) for most up-to-date guidelines and instructions.

## Acknowledgements

This class borrows inspirations from several incredible sources.

- The course syllabus page format loosely follows Xin Jin's [EN.601.414/614](https://github.com/xinjin/course-net) class at John Hopkins.
- The lecture slides' material is partially adapted from my Ph.D. advisors, Jen Rexford's [COS 461](https://www.cs.princeton.edu/courses/archive/fall20/cos461) class and Nick Feamster's [COS 461](https://www.cs.princeton.edu/courses/archive/spring19/cos461/) class at Princeton.
<!-- - [Programming assignment 1](assignments/assignment1) is based on a [similar assignment](https://github.com/PrincetonUniversity/COS461-Public/tree/master/assignments/assignment1) offered at Princeton by Nick Feamster. -->
